module.exports = function(app, db) {

const routeCalculator = require('../../workers/calculators/route_calculator.js');
const parser = require('../../workers/parsers/json_parser.js');
    // POST REQUESTS

    app.post('/calculate/v1', (req, res) => {
        console.log('Calculation request is received - Body: ' + JSON.stringify(req.body));
        let calculator = new routeCalculator(req.body, res);
        calculator.calculate();
    });

    // GET REQUESTS

    app.get('/ph/latest', (req, res) => {
        console.log('Get request for getting latest pH value is received')
        res.send("Request received successfuly")
    });

    app.get('/temp/latest', (req, res) => {
        console.log('Get request for getting latest temperature value is received')
        res.send("Request received successfuly")
    });

    app.get('/ec/latest', (req, res) => {
        console.log('Get request for getting latest e.c value is received')
        res.send("Request received successfuly")
    });

};
