class jsonParser {

    constructor(body) {
        this.body = body;
    }

    parseBody() {
        this.dictBody = this.body.jsonBody;
    };

    parseRoutes() {
        this.parseBody();
        var routes = this.dictBody.routes;
        return routes;
    };

    parseRouteOptions() {
        var options = this.dictBody.options;
        return options;
    }

    parseRouteStays() {
        var stays = this.dictBody.stays;
        return stays;
    }

};

module.exports = jsonParser;