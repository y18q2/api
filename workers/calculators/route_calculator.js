const parser = require('../parsers/json_parser.js');
var G = require('generatorics');
var directions = require('google-maps-direction');

class RouteCalculator {

    constructor(body, sender) {
        this.body = body;
        this.sender = sender;
        let parseEngine = new parser(body);
        this.result = parseEngine.parseRoutes();
        this.options = parseEngine.parseRouteOptions();
        this.startPoint = parseEngine.parseRouteStays();
        this._prepareOptions();
        this.totalRoute = 0;
        this.calculator = 0;
        this.combinations = [];
        this.allResults = [];
        this.timeoutObj = setTimeout(() => {
            this.sender.send({
                "error": "Timeout",
                "x-again": true
            })
            console.log('Timeout reached');
        }, 10000);
        var dp = [];
        for (var i = 0; i < 100; i++) {
            dp[i] = new Array(100);
            for (var j = 0; j < 100; j++) {
                dp[i][j] = new Array(100);
            }
        }

       // console.log(this.getWays(5, 2));
    }

    calculate() {
        var count = 0;
        var pusher = [];
        var length = this.result.length;
        if (this.daysOfRoute > 1) {

        }
        for (let perm of G.permutation(this.result, length)) {
            var a = Object.assign({}, perm)
            this.combinations.push(a);
        }
        for (let key in this.combinations) {
            this._sendMapRequest(this.combinations[key]);
            count++;
        }
        this.totalRoute = count;
    }

    _sendMapRequest(combination) {
        var wPoints = [];
        for (let obj in combination) {
            let x = combination[obj]["latitude"] + ',' + combination[obj]["longitude"];
            wPoints.push(x);
        }
        let stPoint = this.startPoint["latitude"] + ',' + this.startPoint["longitude"]
        //let last = wPoints[wPoints.length - 1];
        //wPoints.pop();
        var that = this;
        directions({
            origin: stPoint,
            destination: stPoint,
            waypoints: wPoints,
            optimizeWaypoints: true,
            alternatives: true,
            travelMode: that.mode
        })
            .then(function (result) {
                that._requestReceived(result, combination);
            }).catch((e) => {
                console.log("Error during route calculation. " + e);
                clearTimeout(that.timeoutObj);
                that.sender.status(500);
                that.sender.send({
                    "error": "Route error. " + e,
                    "x-again": true
                })
            });
    }

    _requestReceived(data, combs) {
        this.calculator++;
        let routes = data.routes;
        let main = routes[0];
        if (main) {
            let legs = main["legs"];
            let dict = legs[0];
            let duration = dict["duration"];
            let durationInSeconds = duration["value"];
            let res = [];
            res.push({
                "route": combs
            })
            res.push({
                "result": durationInSeconds
            })
            res.push({
                "start_end_point": this.startPoint
            })
            res.push({
                "options": this.options
            })
            res.push({
                "route_detail": data
            })
            this.allResults.push(res);
            if (this.calculator == this.totalRoute) {
                this._requestsCompleted();
            }
        }
    }

    _requestsCompleted() {
        var shortestRoute = this.allResults[0];
        for (let key in this.allResults) {
            if (this.allResults[key][1]["result"] < shortestRoute[1]["result"]) {
                shortestRoute = this.allResults[key];
            }
        }

        console.log("Shortest route calculated. Response: ")
        console.log(shortestRoute);
        clearTimeout(this.timeoutObj);
        this.sender.send({ shortestRoute });
    }

    _prepareOptions() {
        this.daysOfRoute = this.options["day"];
        let modeOpt = this.options["mode"];
        switch (modeOpt) {
            case 0:
                this.mode = 'DRIVING'
                break
            case 1:
                this.mode = 'WALKING'
                break
            case 2:
                this.mode = 'TRANSIT'
                break
            case 3:
                this.mode = 'BICYCLING'
                break
        }
    }

    getWays(n, parts) {
        if (parts == 0 && n == 0) return 1;
        if (n <= 0 || parts <= 0) return 0;

        // If this subproblem is already solved
        if (this.dp[n][1][parts] != -1)
            return this.dp[n][1][parts];

        var ans = 0; // Initialize result

        // Count number of ways for remaining number n-i
        // remaining parts "parts-1", and for all part
        // varying from 'nextPart' to 'n'
        for (var i = 1; i <= n; i++) {
            ans += getWays(n - i, parts - 1, i);
        }
        // Store computed answer in table and return
        // result
        return (this.dp[n][1][parts] = ans);
    }

};

module.exports = RouteCalculator;